import processing.video.*;
Movie myMovie;

void setup() {
  fullScreen();
  myMovie = new Movie(this, "Projet.mp4");
  myMovie.play();
}

void draw() {
  image(myMovie, 0, 0);
}

void movieEvent(Movie m) {
  m.read();
}
